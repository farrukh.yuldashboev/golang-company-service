package service

import (
	"context"

	"bitbucket.org/Udevs/position_service/genproto/company_service"
	"bitbucket.org/Udevs/position_service/pkg/helper"
	"bitbucket.org/Udevs/position_service/pkg/logger"
	"bitbucket.org/Udevs/position_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type companyService struct {
	logger  logger.Logger
	storage storage.StorageI
}

func NewCompanyService(db *sqlx.DB, log logger.Logger) *companyService {
	return &companyService{
		logger:  log,
		storage: storage.NewStoragePg(db),
	}
}

func (s *companyService) Create(ctx context.Context, req *company_service.Company) (*company_service.CompanyId, error) {
	id, err := s.storage.Company().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while create company", req, codes.Internal)
	}

	return &company_service.CompanyId{
		Id: id,
	}, nil
}

func (s *companyService) GetAll(ctx context.Context, req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error) {
	resp, err := s.storage.Company().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all companies", req, codes.Internal)
	}

	return resp, nil
}

func (s *companyService) Get(ctx context.Context, req *company_service.CompanyId) (*company_service.Company, error) {
	company, err := s.storage.Company().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting  company", req, codes.Internal)
	}

	return company, nil
}

func (s *companyService) Update(ctx context.Context, req *company_service.Company) (*company_service.Status, error) {
	status, err := s.storage.Company().Update(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while updating  company", req, codes.Internal)
	}

	return &company_service.Status{
		Status: status,
	}, nil
}

func (s *companyService) Delete(ctx context.Context, req *company_service.CompanyId) (*company_service.Status, error) {
	status, err := s.storage.Company().Delete(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while deleting  company", req, codes.Internal)
	}

	return &company_service.Status{
		Status: status,
	}, nil
}
