package storage

import (
	"bitbucket.org/Udevs/position_service/storage/postgres"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Company() repo.CompanyRepoI
}

type storagePg struct {
	db      *sqlx.DB
	company repo.CompanyRepoI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:      db,
		company: postgres.NewCompanyRepo(db),
	}
}

func (s *storagePg) Company() repo.CompanyRepoI {
	return s.company
}
