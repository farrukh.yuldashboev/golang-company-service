package repo

import (
	"bitbucket.org/Udevs/position_service/genproto/company_service"
)

type CompanyRepoI interface {
	Create(req *company_service.Company) (string, error)
	GetAll(req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error)
	Get(id string) (*company_service.Company, error)
	Update(req *company_service.Company) (string, error)
	Delete(id string) (string, error)
}
