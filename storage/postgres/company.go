package postgres

import (
	"database/sql"

	"bitbucket.org/Udevs/position_service/genproto/company_service"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type companyRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewCompanyRepo(db *sqlx.DB) repo.CompanyRepoI {
	return &companyRepo{db: db}
}

func (r *companyRepo) Create(req *company_service.Company) (string, error) {
	var (
		err error
		tx  *sql.Tx
		id  uuid.UUID
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	query := `INSERT INTO company (
				id,
				name
			) 
			VALUES ($1, $2) `

	_, err = tx.Exec(query, id, req.Name)

	if err != nil {
		return "", err
	}

	return id.String(), nil
}

func (r *companyRepo) GetAll(req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error) {
	var (
		filter    string
		args      = make(map[string]interface{})
		count     int32
		companies []*company_service.Company
	)

	if req.Name != "" {
		filter += " AND name ilike '%' || :name || '%' "
		args["name"] = req.Name
	}

	countQuery := `SELECT count(1) FROM company WHERE true ` + filter
	rows, err := r.db.NamedQuery(countQuery, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			return nil, err
		}
	}

	query := `SELECT
					id,
					name
				FROM company WHERE true ` + filter

	rows, err = r.db.NamedQuery(query, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var company company_service.Company

		err = rows.Scan(
			&company.Id,
			&company.Name,
		)

		if err != nil {
			return nil, err
		}

		companies = append(companies, &company)
	}

	return &company_service.GetAllCompanyResponse{
		Companies: companies,
		Count:     count,
	}, nil

}

func (r *companyRepo) Get(id string) (*company_service.Company, error) {
	var company company_service.Company

	query := `SELECT id, name FROM company WHERE id = $1`

	row := r.db.QueryRow(query, id)
	err := row.Scan(
		&company.Id,
		&company.Name,
	)

	if err != nil {
		return nil, err
	}

	return &company, nil
}

func (r *companyRepo) Update(req *company_service.Company) (string, error) {
	var (
		err error
		tx  *sql.Tx
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	query := `
		UPDATE company
		SET name = $1
		WHERE id = $2
	`

	_, err = tx.Exec(query, req.Name, req.Id)

	if err != nil {
		return "", err
	}

	return "updated", nil
}

func (r *companyRepo) Delete(id string) (string, error) {
	query := `DELETE FROM company WHERE id = $1`

	_, err := r.db.Exec(query, id)

	if err != nil {
		return "", err
	}

	return "deleted", nil
}
